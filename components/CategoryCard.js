import { useContext } from 'react'
import UserContext from '../UserContext'
import ArchiveButton from './ArchiveButton'
import UpdateButton from './UpdateButton'

export default function CategoryCard({categoryData}) { 
    const { user } = useContext(UserContext)
    return (
    	<>
			<tr>	
		      <td key={categoryData._id}>{categoryData.name}</td>	
		      <td key={categoryData._id}>{categoryData.type}</td>
		      <td><UpdateButton categoryId={categoryData._id}/> <ArchiveButton categoryId={categoryData._id} isActive={categoryData.isActive} /></td>
	      	</tr>

    	</>	
    )
}