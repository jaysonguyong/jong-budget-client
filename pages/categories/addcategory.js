import { useState, useEffect, useContext } from 'react'
import { Form, Alert, Jumbotron, Container, Row, Col } from 'react-bootstrap'
import {Button} from '@material-ui/core'
import Router from 'next/router'
import UserContext from '../../UserContext'


export default function addcategory() {
    const { user } = useContext(UserContext)
    const [name, setName] = useState('')
    const [type, setType] = useState('')
    const [isActive, setIsActive] = useState(false) 
    const [notify, setNotify] = useState(false)

console.log(user)

    useEffect(() => {
        if(name.length < 50){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    }, [name])

    function add(e){
        e.preventDefault()

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                type: type
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if(data===true){
                Router.push('/categories')
            }else{
                setNotify(true)
            }
        })
    }

   
         return (
            <>
            <Container fluid>
                <Row>
                    <Col xs={12} id="add-category-first-layer">
                        <Row>
                            <Col xs={12} md={{ span: 6, offset: 3 }}   id="add-category-second-layer">
                                <Form onSubmit={(e) => add(e)}>
                                    <Form.Group>
                                        <Form.Label className="text-white">Category Name:</Form.Label>
                                        <Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required />
                                        {name.length >= 50 ? <Alert variant="warning">Name has exceeded maximum length</Alert>:null}
                                    </Form.Group>
                                    <Form.Group>
                                        <Form.Label className="text-white">Category Type:</Form.Label>
                                        <Form.Control type="text" onChange={e => setType(e.target.value)} as="select" cutom required>
                                        <option value="" disabled selected>choose below</option>
                                        <option value="income">income</option>
                                        <option value="expense">expense</option>
                                        </Form.Control>
                                    </Form.Group>
                                    {isActive===true
                                    ? <Button type="submit" variant="contained" color="primary">add</Button>
                                    : <Button disabled type="submit" variant="contained" color="primary">add</Button>}
                                </Form>

                                {notify === true
                                ? <Alert variant="danger">Failed to create a Category!</Alert>
                                : null}
                            </Col>
                        </Row>
                    </Col>
                </Row>    
            </Container>   

            </>
        )

   

}

