import { useState, useEffect, useContext } from 'react'
import { Jumbotron, Row, Col, Container, Table  } from 'react-bootstrap'
import CategoryCard from '../../components/CategoryCard'
import UserContext from '../../UserContext'



export async function getServerSideProps() { 
    const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/categories`)
    const data = await res.json()

    //use the pre-fetched data as a prop to this component that will be pre-rendered
    return {
        props: {
            data
        }
    }
}


export default function index({data}) {
	console.log(data)
    const { user } = useContext(UserContext)
    const [activeCategories, setActiveCategories] = useState([])

    useEffect(() => {
        if(data.length > 0){
            setActiveCategories(data.filter(category => category.isActive === true))
        }
    }, [data])
        console.log(setActiveCategories)

    if(data.length === 0){
        return (
            <Jumbotron>
                <h1>There are no available categories yet</h1>
            </Jumbotron>
        )
    }else{
        if(activeCategories.length > 0){
            return (
                        <Container fluid id="login-background">
                            <Row>
                                <Col xs={12}  id="category-first-layer">
                                    <Row>
                                        <Col xs={12} md={{ span: 8, offset: 2 }} id="category-second-layer">
                                            <Table striped bordered hover size="lg" responsive="md">
                                                <thead>
                                                    <tr>
                                                        <th>Category Name</th>
                                                        <th>Category type</th>
                                                        <th>Actions</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    {activeCategories.map(category => {
                                                    
                                                        return (
                                                       <CategoryCard categoryData={category}/>
                                                        )
                                                     
                                                     })} 
                                                </tbody>      
                                             </Table>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </Container>
                     )
            }else{
                return (
                    <Jumbotron>
                        <h1>There are no courses on offer as of the moment.</h1>
                    </Jumbotron>
                )
            }
    }
} 

//pre-fetch data for pre-rendering our component
//use getServerSideProps so that the fetch request will be sent to the API on every request sent to this page

