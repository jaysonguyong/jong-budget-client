import { useState, useEffect } from 'react'
import { Form, Button,Container, Row, Col } from 'react-bootstrap'
import Router from 'next/router'

export default function register() {
    //form input state hooks
    const [email, setEmail] = useState('')
    const [password1, setPassword1] = useState('')
    const [password2, setPassword2] = useState('')
    

    //state to determine the status of the form submission button
    const [isActive, setIsActive] = useState(false)

    //for any functionality that you want triggered based on a change of state, the useEffect hook is the tool of choice
    useEffect(() => {
        //additional validations for the password and mobile no fields
        if((password1 !== '' && password2 !== '') && (password2 === password1)){
            setIsActive(true)
        }else{
            setIsActive(false)
        }
    }, [password1, password2])


    //function for submitting user registration form
    function registerUser(e){
        e.preventDefault()

        //send fetch request to API endpoint responsible for creating a new user
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password1
            })
        })
        .then(res => res.json())
        .then(data => {
            //if registration is successful, redirect to login
            if(data === true){
                Router.push('/login')
            }else{//redirect to an error page otherwise
                Router.push('/error')
            }
        })
    }


    return (    
        <Container fluid>
            <Row>
                <Col xs={12} id="register-first-layer">
                    <Row>
                        <Col xs={12} md={{ span: 4, offset: 1 }} id="register-second-layer">

                            <Form onSubmit={(e) => registerUser(e)}>
                                <Form.Group>
                                    <Form.Label>Email</Form.Label>
                                    <Form.Control type="email" value={email} onChange={e => setEmail(e.target.value)} required></Form.Control>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control type="password" value={password1} onChange={e => setPassword1(e.target.value)} required></Form.Control>
                                </Form.Group>
                                <Form.Group>
                                    <Form.Label>Verify Password</Form.Label>
                                    <Form.Control type="password" value={password2} onChange={e => setPassword2(e.target.value)} required></Form.Control>
                                </Form.Group>
                                {isActive === true 
                                ? <Button variant="success" type="submit">Submit</Button> 
                                : <Button variant="success" type="submit" disabled>Submit</Button>}
                            </Form>
                        </Col>
                     </Row>
                </Col>
            </Row>
        </Container>
    )
}
